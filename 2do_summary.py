# coding: utf-8

import appex
import csv
import console
from datetime import datetime, timedelta

CHECK_DATE = datetime.today().date()
DATE_FORMAT = '%d.%m.%Y, %H:%M'

def main():
    if not appex.is_running_extension():
        print('This script is intended to be run from the sharing extension.')
        return
    csv_file_path = appex.get_file_path()
    if csv_file_path is None:
        print('No file path')
        return
    with open(csv_file_path, 'r', encoding='utf-8-sig', newline='') as csv_file:
        total = 0
        undone = 0
        added_today = 0
        done_today = 0
        for row in csv.DictReader(csv_file, skipinitialspace=True):
            total += 1
            if row['COMPLETED']:
                completion_date = datetime.strptime(row['COMPLETIONDATE'], DATE_FORMAT).date()
                if completion_date == CHECK_DATE:
                    done_today += 1
            else:
                undone += 1
            created_date = datetime.strptime(row['CREATED'], DATE_FORMAT).date()
            if created_date == CHECK_DATE:
                added_today +=1
    console.alert('Summary', 
    f'{CHECK_DATE}\n'
    f'Total: {total}\n'
    f'Undone: {undone}\n'
    f'Today added: {added_today}\n'
    f'Today done: {done_today}',
    'OK', hide_cancel_button=True)

if __name__ == '__main__':
    main()

