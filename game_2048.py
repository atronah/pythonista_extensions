from scene import run, Scene, LabelNode, ShapeNode, Node, Point
from ui import Path
from itertools import product
from numpy import zeros, rot90, nditer
from datetime import datetime
from random import choices


class Cell(ShapeNode):
    def __init__(self, cell_size, *argv, **kwargs):
        path = Path.rounded_rect(0, 0, cell_size, cell_size, 10)
        super().__init__(path, fill_color='clear', stroke_color='white', *argv, **kwargs)
        self._value = None
        self._label = LabelNode(color='black')
        self.add_child(self._label)
        self._color = 'clear'
        
    
    @property
    def value(self):
        return self._value
    
    @value.setter
    def value(self, value):
        self._value = value
        self._label.text = f'{self._value or ""}'
        self.fill_color = {'': 'clear',
                           '2': '#fee4e3',
                           '4': '#cbfecb',
                           '8': '#e4e4f3',
                           '16': '#c9c9ff',
                           '32': '#6efe6f',
                           '64': '#ffc9c7',
                           '128': '#ffa8ac',
                           '256': '#adfcaa',
                           '512': '#ada8ff'
                           }.get(self._label.text, 'white')

            
class Field(Node):
    def __init__(self, rows, columns, cell_size, margin = 5, *argv, **kwargs):
        self._rows = rows
        self._columns = columns
        self._cell_size = cell_size
        self._margin = margin
        self._cells = {}
        super().__init__(*argv, **kwargs)
        for row, column in product(range(self._rows), range(self._columns)):
            cell = Cell(self._cell_size)
            pos = (cell.frame.size + (self._margin, self._margin)) / 2
            pos += cell.frame.size * (column, self._rows - row - 1)
            cell.position = pos
            self._cells[(row, column)] = cell
            self.add_child(cell)
    
    @property
    def cells(self):
        return self._cells


class GameCore(object):
    BASE = 2
    
    class IncorectShape(Exception):
        pass
        
    class ValueExistsError(Exception):
        pass
    
    class IncorrectValueError(Exception):
        pass
    
    class IncorrectCellAddressError(Exception):
        pass
        
    
    @classmethod
    def set_value(cls, data, row, column, value = None):
        power = log(value, GameCore.BASE) if value else 0
        if power != int(power):
            raise cls.IncorrectValueError
        
        if row > data.shape[0] or column > data.shape[1]:
            raise cls.IncorrectCellAddressError
        
        data[row][column] = value or 0
    
    @staticmethod
    def increase_value(data, row, column):
        new_value = data[row][column] * 2 or 2
        data[row][column] = new_value
        return new_value
        
    @property
    def score(self):
        return self._score
    
    @staticmethod
    def values(data, only_free = False):
        for r, c in product(*map(range, data.shape)):
            if data[r][c] and only_free:
                continue
            yield (r, c, data[r][c])
    
    @classmethod
    def append_new(cls, data):
        empty_values = list(cls.values(data, only_free=True))
        if empty_values:
            address = choices(empty_values)[0][:2]
            new_value = choices((GameCore.BASE, GameCore.BASE ** 2), (9, 1))[0]
            data[address] = new_value
    
    @classmethod
    def squeeze(cls, arr, filler=0):
            squeezed_arr = []
            squeezed_sum = squeezed_cnt = skipped = 0
            has_been_squeezed = False
            for val in arr:
                if val == filler:
                    skipped += 1
                elif not squeezed_arr or has_been_squeezed:
                    squeezed_arr.append(val)
                    has_been_squeezed = False
                elif val == squeezed_arr[-1]:
                    squeezed_arr[-1] += val
                    squeezed_sum += squeezed_arr[-1]
                    squeezed_cnt += 1
                    has_been_squeezed = True
                else:
                    squeezed_arr.append(val)
                    has_been_squeezed = False
                
            return (squeezed_arr + [filler, ] * (skipped + squeezed_cnt), squeezed_sum, squeezed_cnt, skipped)
    
    @classmethod
    def move_values(cls, data, direction):
        new_data, points, purge_count = data.copy(), 0, 0
        direction_list = ['left', 'up', 'right', 'down']
        rotations = direction_list.index(direction)
        new_data = rot90(new_data, rotations)
        
        for row in range(new_data.shape[0]):
            new_data[row, :], squeezed_sum, squeezed_cnt, _ = cls.squeeze(new_data[row, :])
            points += squeezed_sum
            purge_count += squeezed_cnt
        new_data = rot90(new_data, len(direction_list) - rotations)
        return (new_data, points, purge_count)
        
        

class Game(Scene):
    CELL_SIZE = 50
    MARGIN = 5
    FIELD_ROWS = 4
    FIELD_COLUMNS = 4
    
    def setup(self):
        self.field = Field(Game.FIELD_ROWS, Game.FIELD_COLUMNS, Game.CELL_SIZE, Game.MARGIN)
        self.field.position = self.bounds.center() - self.field.bbox.size / 2
        self.add_child(self.field)
        self.info = LabelNode(f'{self.size.w} x {self.size.h}', position=(self.size.w/2, self.size.h-50),parent=self)
        self.field_data = zeros((Game.FIELD_ROWS, Game.FIELD_COLUMNS), int)
        self.score = 0
        self.touch_started_time = None
        
        fbox = self.field.bbox
        self.prediction_info = {'left': LabelNode(position=(fbox.x/2, fbox.y + fbox.h/2)),
                                'up': LabelNode(position=(fbox.x + fbox.w/2, self.bounds.h - fbox.h/2)),
                                'right': LabelNode(position=(self.bounds.w - fbox.w/4, fbox.y + fbox.h/2)),
                                'down': LabelNode(position=(fbox.x + fbox.w/2, fbox.y/2))
                                }
        for key, info in self.prediction_info.items():
            self.add_child(info)
            info.text = key
            
        
        
    def update_field(self):
        for r, c, v in GameCore.values(self.field_data):
            self.field.cells[r,c].value = v
    
    def touch_began(self, touch):
        self.info.text = 'begun'
        self.touch_started_time = datetime.now()
        
    def touch_moved(self, touch):
        self.info.text = str(touch.location)
    
    def touch_ended(self, touch):
        self.info.text = 'end'
        touch_duration = (datetime.now() - self.touch_started_time) if self.touch_started_time else 0
        self.touch_started_time = None
        
        if self.field.bbox.contains_point(self.field.parent.point_from_scene(touch.location)):
            for (r, c), cell in self.field.cells.items():
                if cell.frame.contains_point(cell.parent.point_from_scene(touch.location)):
                    if touch_duration.microseconds > 500000:
                        GameCore.set_value(self.field_data, r, c, None)
                    else:
                        GameCore.increase_value(self.field_data, r, c)
        else:
            direction = None
            touch_point = self.field.parent.point_from_scene(touch.location)
            field_rect = self.field.bbox
            
            if field_rect.y <= touch_point.y <= (field_rect.y + field_rect.h):
                if touch_point.x < field_rect.x:
                    direction = 'left'
                elif touch_point.x > field_rect.x + field_rect.w:
                    direction = 'right'
            elif field_rect.x <= touch_point.x <= (field_rect.x + field_rect.w):
                if touch_point.y < field_rect.y:
                    direction = 'down'
                elif touch_point.y > field_rect.y + field_rect.h:
                    direction = 'up'
            if direction:
                self.field_data, points, _ = GameCore.move_values(self.field_data, direction)
                self.score += points
                self.info.text = str(self.score)
                GameCore.append_new(self.field_data)
            
        self.update_field()
        


if __name__ == '__main__':
    run(Game())
