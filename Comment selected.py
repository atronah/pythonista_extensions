#Comment/Uncomment selected lines

import editor

text = editor.get_text()
selection = editor.get_line_selection()
selected_text = text[selection[0]:selection[1]]
replacement_lines = []

for line in selected_text.splitlines():
    replacement_lines.append()

replacement = '\n'.join((line[1:] if line.startswith('#') else '#' + line) for line in replacement_lines)
editor.replace_text(selection[0], selection[1], replacement)
editor.set_selection(selection[0], selection[0] + len(replacement) - 1)
