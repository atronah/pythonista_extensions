import os
import sys
from datetime import datetime, timedelta
from random import random
import itertools
from configparser import ConfigParser
from collections import OrderedDict

import ui
import objc_util
import appex


config_filename = os.path.splitext(os.path.basename(sys.argv[0]))[0] + '.conf'

options = {'log_filename': 'passed_time.log',
           'datetime_format': '%Y-%m-%d %H:%M:%S',
           'margin': 4
           }

events = OrderedDict()

def reload_events():
    global options
    
    event_name_list = []
    if not os.path.isfile(config_filename):
        return result
    conf = ConfigParser()
    conf.read(config_filename, encoding='utf-8')
    
    for name in conf.sections():
        if name == 'general':
            options.update(conf[name])
        else:
            event_info = events.setdefault(name, {'title': conf[name].get('title', name)})
    
    for name in events:
        if name not in conf.sections():
            del events[name]
        

def reload_statistics():
    global options
    log_filename = options['log_filename']
    if not os.path.isfile(log_filename):
        return
    
    for event_info in events.values():
        if 'appears' in event_info:
            del event_info['appears']
    
    with open(log_filename, 'r') as s:
        for line in s:
            event_name, event_date = decode_log_line(line.strip('\n'))
            if event_name not in events:
                continue
            event_info = events[event_name]
            appears = event_info.setdefault('appears', [])
            if event_date not in appears:
                appears.append(event_date)
            

def format_period(start_date, end_date):
    delta = end_date - start_date
    result = [(delta.days // 7, 'w'),
              (delta.days % 7, 'd'),
              ((delta.seconds // (60*60)) % 24, 'h'),
              ((delta.seconds // 60) % 60, 'm'),
              (delta.seconds % 60, 's')
              ]
    return ' '.join([str(d)+s for d, s in result if d > 0][:2])


def make_default_button(name):
    global options
    b = ui.Button(name=name)
    b.background_color = options['button_color']
    b.corner_radius = 12
    b.border_width = 1
    b.border_color = 'white'
    
    b.width = ui.get_window_size()[0]
    b.height = 30
    
    NSLineBreakByWordWrapping = 0
    NSLineBreakByCharWrapping = 1
    NSLineBreakByClipping = 2
    NSLineBreakByTruncatingHead = 3
    NSLineBreakByTruncatingTail = 4
    NSLineBreakByTruncatingMiddle = 5 # Default
    objc_util.ObjCInstance(b).button().titleLabel().setLineBreakMode(NSLineBreakByWordWrapping)
    
    return b


def make_event_button(name):
    b = make_default_button(name)
    b.action = tap_event
    b.width, b.height = 114, 50
    return b
    
    
def make_events_view():
    reload_events()
    reload_statistics()
    
    global options
    margin = int(options['margin'])
    
    view = ui.View()
    for idx, name in enumerate(events):
        event_info = events[name]
        title = event_info.get('title', name)
        appears = sorted(event_info.get('appears', []))
        passed = format_period(appears[-1], datetime.now()) if appears else ''
        count = len(appears)
        
        b = make_event_button(name)
        b.title = f'{title}\n{passed}({count})'
        b.x = margin + (b.width + margin) * (idx % 3)
        b.y = ((b.height + margin) * (idx // 3)) + ((idx // 3) + 1)
        view.add_subview(b)
        view.width = max(view.width, 
                        b.x + b.width + margin)
        view.height = max(view.height, 
                         b.y + b.height + margin)
    
    return view
    

def make_event_info_view(name):
    reload_statistics()
    
    global options
    margin = int(options['margin'])
    datetime_format = options['datetime_format']
    
    view = ui.View()
    view.width, view.height = ui.get_window_size()
    view.height += 500
    
    event_info = events[name]
    title = event_info.get('title', name)
    
    appears = sorted(event_info.get('appears', []), reverse=True)
    
    label = ui.Label(name='label')
    label.width = view.width
    label.height = 30
    label.x = margin
    label.y = margin
    label.text = title or name
    label.content_mode = ui.CONTENT_SCALE_TO_FILL
    label.alignment = ui.ALIGN_CENTER
    view.add_subview(label)
    
    lds = ui.ListDataSource([dt.strftime(datetime_format) for dt in appears])
    lds.event_name = name
    lds.edit_action = delete_items
    tv = ui.TableView(name='event_')
    tv.width = 250 
    tv.height = 45 * len(appears)
    tv.x = margin
    tv.y = label.y + label.height + margin
    tv.data_source = lds
    tv.editing = True
    view.add_subview(tv)
    view.height = tv.y + tv.height
    
    log = make_default_button(name)
    log.title = '+'
    log.action = log_event
    log.width = view.width - tv.width - margin * 3
    log.x = margin + tv.width
    log.y = tv.y
    log.flex = 'LB'
    view.add_subview(log)
    
    close = make_default_button('close')
    close.title = 'close'
    close.action = close_event_info
    close.width = log.width
    close.x = log.x
    close.y = view.height - close.height - margin
    close.flex = 'T'
    view.add_subview(close)
    
    return view


def show_view(view):
    if appex.is_widget():
        appex.set_widget_view(view)
    elif __name__ == '__main__':
        view.background_color = 'white'
        view.present('full_screen')


def tap_event(sender):
    view = make_event_info_view(sender.name)
    show_view(view)


def close_event_info(sender):
    view = make_events_view()
    show_view(view)
    
    
def decode_log_line(line):
    global options
    datetime_format = options['datetime_format']
    
    idx = line.index(':')
    event_name = line[:idx]
    datetime_str = line[idx+1:].strip()
    event_date = datetime.strptime(datetime_str, datetime_format)
    return (event_name, event_date)


def encode_log_line(event_name, event_date):
    global options
    datetime_format = options['datetime_format']
    
    if isinstance(event_date, datetime):
        event_date = event_date.strftime(datetime_format)
    return f'{event_name}:{event_date}'


def log_event(sender):
    global options
    log_filename = options['log_filename']
    
    if sender.name:
        with open(log_filename, 'a') as target:
            event_date = datetime.now()
            line = encode_log_line(sender.name, event_date)
            target.write(line + '\n')
    close_event_info(sender)
    

def delete_items(data_source):
    global options
    datetime_format = options['datetime_format']
    
    remaining_apears = [datetime.strptime(dts if isinstance(dts, str) else dts['title'], datetime_format) for dts in data_source.items]
    
    event_name = data_source.event_name
    bak_suffix = datetime.now().strftime('_bak%Y%m%d_%H%M%S')
    bak_filename = bak_suffix.join(os.path.splitext(log_filename))
    os.rename(log_filename, bak_filename)
    with open(bak_filename, 'r') as bak_log, open(log_filename, 'a') as new_log:
        for line in bak_log:
            bak_event_name, bak_event_date = decode_log_line(line)
            if bak_event_name == event_name:
                if bak_event_date not in remaining_apears:
                    continue
            new_log.write(line)
        
    

view = make_events_view()
show_view(view)
