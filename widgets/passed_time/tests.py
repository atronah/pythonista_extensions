from datetime import datetime, timedelta
from passed_time import format_period

def test_format_period():
    d = datetime.now()
    assert format_period(d, d + timedelta(minutes=3, seconds=17)) == '3m 17s'
    assert format_period(d, d + timedelta(days=3)) == '3d'
    assert format_period(d, d + timedelta(days=6, hours=23)) == '6d 23h'
    assert format_period(d, d + timedelta(days=6, hours=24)) == '1w'
    assert format_period(d, d + timedelta(weeks=6)) == '6w'
    assert format_period(d, d + timedelta(weeks=2, days=3)) == '2w 3d'
    assert format_period(d, d + timedelta(weeks=1, days=2, hours=3)) == '1w 2d'
